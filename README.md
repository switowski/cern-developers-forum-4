# I made a website! Now what?

Presentation for 4rd Developers Forum at CERN (Geneva, 23-24th October 2017)

To view the slides in the browser, go [here](https://switowski.gitlab.io/cern-developers-forum-4/#/)

To see the video from the presentation, go [here](https://cds.cern.ch/record/2290762)

PDF version of the slides is available in the *Slides.pdf* file (but slides look way better in the browser)

## Important

This repository will no longer be updated.
